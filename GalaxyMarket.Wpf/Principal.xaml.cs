﻿using GalaxyMarket.Wpf.Views.Clientes;
using GalaxyMarket.Wpf.Views.Produto;
using GalaxyMarket.Wpf.Views.Vendedor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GalaxyMarket.Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void buttonClientes_Click(object sender, RoutedEventArgs e)
        {
            var opcoesCliente = new OpcoesCliente();
            opcoesCliente.ShowDialog();
        }

        private void buttonVendedores_Click(object sender, RoutedEventArgs e)
        {
            var opcoesVendedor = new OpcoesVendedor();
            opcoesVendedor.ShowDialog();
        }

        private void buttonProdutos_Click(object sender, RoutedEventArgs e)
        {
            var opcoesProduto = new OpcoesProduto();
            opcoesProduto.ShowDialog();

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBox.Show("Deseja mesmo sair?", "Saindo...", 
               MessageBoxButton.YesNo, MessageBoxImage.Question) ==
               MessageBoxResult.No)
            {
                e.Cancel = true;
            }
        }

    }
}
