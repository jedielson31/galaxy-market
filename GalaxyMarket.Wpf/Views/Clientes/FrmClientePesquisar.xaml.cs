﻿using GalaxyMarket.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GalaxyMarket.Wpf.Views.Clientes
{
    /// <summary>
    /// Interaction logic for FrmClientePesquisar.xaml
    /// </summary>
    public partial class FrmClientePesquisar : Window
    {
        public FrmClientePesquisar()
        {
            InitializeComponent();
        }
        private void LimparForm()
        {
            textBoxNome.Text = string.Empty;
            textBoxCpf.Text = string.Empty;
            textBoxCep.Text = string.Empty;
            textBoxEmail.Text = string.Empty;
            comboBoxSexo.SelectedIndex = -1;
            comboBoxStatus.SelectedIndex = -1;
            DatePickerNascimento.SelectedDate = null;
        }

        private void buttonLimpar_Click(object sender, RoutedEventArgs e)
        {
            LimparForm();
        }

        private void buttonCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            comboBoxSexo.ItemsSource = Enum.GetValues(typeof(Enumerations.Sexo)).Cast<Enumerations.Sexo>();
            comboBoxStatus.ItemsSource = Enum.GetValues(typeof(Enumerations.Status)).Cast<Enumerations.Status>();
        }
    }
}
