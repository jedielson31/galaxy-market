﻿using GalaxyMarket.Application;
using GalaxyMarket.Domain.Model;
using GalaxyMarket.Domain.Services;
using GalaxyMarket.Infra.Data.Repository.EntityFramework;
using GalaxyMarket.Wpf.Validations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GalaxyMarket.Wpf.Views.Clientes
{
    /// <summary>
    /// Interaction logic for FrmClienteEditar.xaml
    /// </summary>
    public partial class FrmClienteEditar : Window
    {
        public FrmClienteEditar()
        {
            InitializeComponent();
        }

        private void Editar()
        {
            try
            {
                ValidaTela();
                var cliente = LeForm();

                using (var repo = new ClienteRepository())
                {
                    using (var service = new ClienteService(repo))
                    {
                        using (var appService = new ClienteAppService(service))
                        {
                            //appService.  (cliente);
                        }
                    }
                }

                MessageBox.Show("Cliente cadastrado com sucesso.", "Sucesso", MessageBoxButton.OK,
                    MessageBoxImage.Asterisk);

            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ocorreu um erro ao salvar o cliente{Environment.NewLine}{ex.Message}", "Erro",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private Cliente LeForm()
        {
            var nome = textBoxNome.Text;
            var cpf = textBoxCpf.Text;
            var cep = textBoxCep.Text;

            // ReSharper disable once PossibleInvalidOperationException
            DateTime dataDeNascimento = (DateTime)DatePickerNascimento.SelectedDate;

            Enumerations.Sexo sexo = (Enumerations.Sexo)comboBoxSexo.SelectedValue;
            var email = textBoxEmail.Text;
            var cliente = new Cliente(nome, cpf, cep, dataDeNascimento, sexo, email);
            return cliente;
        }

        private void ValidaTela()
        {
            if (textBoxNome.HasText())
            {
                throw new Exception("O nome é obrigatório");
            }

            if (textBoxCpf.HasText())
            {
                throw new Exception("O Cpf é obrigatório");
            }

            if (comboBoxSexo.HasValueSelected())
            {
                throw new Exception("O Sexo é obrigatório");
            }
        }

        private void buttonCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            comboBoxSexo.ItemsSource = Enum.GetValues(typeof(Enumerations.Sexo)).Cast<Enumerations.Sexo>();
            comboBoxStatus.ItemsSource = Enum.GetValues(typeof(Enumerations.Status)).Cast<Enumerations.Status>();
        }
    }
}
