﻿using GalaxyMarket.Wpf.Views.Clientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GalaxyMarket.Wpf.Views.Clientes
{
    /// <summary>
    /// Interaction logic for OpcoesCliente.xaml
    /// </summary>
    public partial class OpcoesCliente : Window
    {
        public OpcoesCliente()
        {
            InitializeComponent();
        }

        private void buttonCadastrar_Click(object sender, RoutedEventArgs e)
        {
            var frmClienteCadastrar = new Clientes.FrmClienteCadastrar();
            frmClienteCadastrar.ShowDialog();
        }

        private void buttonPesquisar_Click(object sender, RoutedEventArgs e)
        {
            var frmClientePesquisar = new FrmClientePesquisar();
            frmClientePesquisar.ShowDialog();
        }

        private void buttonCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
