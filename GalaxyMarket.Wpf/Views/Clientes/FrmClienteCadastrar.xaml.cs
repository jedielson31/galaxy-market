﻿using System;
using System.Linq;
using System.Windows;
using GalaxyMarket.Application;
using GalaxyMarket.Domain.Model;
using GalaxyMarket.Domain.Services;
using GalaxyMarket.Infra.Data.Repository.EntityFramework;
using GalaxyMarket.Wpf.Validations;

namespace GalaxyMarket.Wpf.Views.Clientes
{
    /// <summary>
    /// Interaction logic for FrmClienteCadastrar.xaml
    /// </summary>
    // ReSharper disable once RedundantExtendsListEntry
    public partial class FrmClienteCadastrar : Window
    {
        public FrmClienteCadastrar()
        {
            InitializeComponent();
        }

        private void LimparForm()
        {
            textBoxNome.Text = string.Empty;
            textBoxCpf.Text = string.Empty;
            textBoxCep.Text = string.Empty;
            textBoxEmail.Text = string.Empty;
            comboBoxSexo.SelectedIndex = -1;
            DatePickerNascimento.SelectedDate = null;
            //comboBoxStatus.SelectedIndex = -1;
        }

        private void Salvar()
        {
            try
            {
                ValidaTela();
                var cliente = LeForm();

                using (var repo = new ClienteRepository())
                {
                    using (var service = new ClienteService(repo))
                    {
                        using (var appService = new ClienteAppService(service))
                        {
                            appService.Create(cliente);
                        }
                    }
                }

                MessageBox.Show("Cliente cadastrado com sucesso.", "Sucesso", MessageBoxButton.OK,
                    MessageBoxImage.Asterisk);

            }
            catch (Exception ex)
            {
                var message = $"Ocorreu um erro ao salvar o cliente{Environment.NewLine}";
                var inner = ex.InnerException;
                while (inner != null)
                {
                    message += $"{inner.Message}{Environment.NewLine}";
                    inner = inner.InnerException;
                }
                MessageBox.Show(message, "Erro", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private Cliente LeForm()
        {
            var nome = textBoxNome.Text;
            var cpf = textBoxCpf.Text;
            var cep = textBoxCep.Text;

            // ReSharper disable once PossibleInvalidOperationException
            DateTime idade = (DateTime)DatePickerNascimento.SelectedDate;

            Enumerations.Sexo sexo = (Enumerations.Sexo)comboBoxSexo.SelectedValue;
            var email = textBoxEmail.Text;
            var cliente = new Cliente(nome, cpf, cep, idade, sexo, email);
            return cliente;
        }

        private void ValidaTela()
        {
            if (!textBoxNome.HasText())
            {
                throw new Exception("O nome é obrigatório");
            }

            if (!textBoxCpf.HasText())
            {
                throw new Exception("O Cpf é obrigatório");
            }

            if (!comboBoxSexo.HasValueSelected())
            {
                throw new Exception("O Sexo é obrigatório");
            }

            if (!DatePickerNascimento.HasDateSelected())
            {
                throw new Exception("A data de nascimento é obrigatória");
            }
        }

        private void buttonLimpar_Click(object sender, RoutedEventArgs e)
        {
            LimparForm();
        }

        private void buttonCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            comboBoxSexo.ItemsSource = Enum.GetValues(typeof(Enumerations.Sexo)).Cast<Enumerations.Sexo>();
            //comboBoxStatus.ItemsSource = Enum.GetValues(typeof(Enumerations.Status)).Cast<Enumerations.Status>();
        }

        private void ButtonSalvar_OnClick(object sender, RoutedEventArgs e)
        {
            Salvar();
        }
    }
}
