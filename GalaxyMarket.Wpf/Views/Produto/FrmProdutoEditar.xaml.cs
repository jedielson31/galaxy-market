﻿using GalaxyMarket.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GalaxyMarket.Wpf.Views.Produto
{
    /// <summary>
    /// Interaction logic for FrmProdutoEditar.xaml
    /// </summary>
    public partial class FrmProdutoEditar : Window
    {
        public FrmProdutoEditar()
        {
            InitializeComponent();
        }

        private void buttonCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            comboBoxTipoProduto.ItemsSource = Enum.GetValues(typeof(Enumerations.Produto)).Cast<Enumerations.Produto>();
            comboBoxStatus.ItemsSource = Enum.GetValues(typeof(Enumerations.Status)).Cast<Enumerations.Status>();
        }
    }
}
