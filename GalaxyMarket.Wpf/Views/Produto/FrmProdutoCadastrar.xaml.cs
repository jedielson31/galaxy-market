﻿using GalaxyMarket.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GalaxyMarket.Wpf.Views.Produto
{
    /// <summary>
    /// Interaction logic for FrmProdutoCadastrar.xaml
    /// </summary>
    public partial class FrmProdutoCadastrar : Window
    {
        public FrmProdutoCadastrar()
        {
            InitializeComponent();
        }

        private void LimparForm()
        {
            textBoxNome.Text = string.Empty;
            textBoxValorCompra.Text = string.Empty;
            textBoxValorMarkup.Text = string.Empty;
            comboBoxTipoProduto.SelectedIndex = -1;
            comboBoxStatus.SelectedIndex = -1;
        }

        private void buttonLimpar_Click(object sender, RoutedEventArgs e)
        {
            LimparForm();
        }

        private void buttonCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            comboBoxTipoProduto.ItemsSource = Enum.GetValues(typeof(Enumerations.Produto)).Cast<Enumerations.Produto>();
            comboBoxStatus.ItemsSource = Enum.GetValues(typeof(Enumerations.Status)).Cast<Enumerations.Status>();
        }
    }
}
