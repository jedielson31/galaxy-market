﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GalaxyMarket.Wpf.Views.Vendedor
{
    /// <summary>
    /// Interaction logic for OpcoesVendedor.xaml
    /// </summary>
    public partial class OpcoesVendedor : Window
    {
        public OpcoesVendedor()
        {
            InitializeComponent();
        }

        private void buttonCadastrar_Click(object sender, RoutedEventArgs e)
        {
            var frmVendedorCadastrar = new FrmVendedorCadastrar();
            frmVendedorCadastrar.ShowDialog();
        }

        private void buttonPesquisar_Click(object sender, RoutedEventArgs e)
        {
            var frmVendedorPesquisar = new FrmVendedorPesquisar();
            frmVendedorPesquisar.ShowDialog();
        }

        private void buttonCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
