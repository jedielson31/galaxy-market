﻿using GalaxyMarket.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GalaxyMarket.Wpf.Views.Vendedor
{
    /// <summary>
    /// Interaction logic for FrmVendedorEditar.xaml
    /// </summary>
    public partial class FrmVendedorEditar : Window
    {
        public FrmVendedorEditar()
        {
            InitializeComponent();
        }

        private void buttonCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            comboBoxSexo.ItemsSource = Enum.GetValues(typeof(Enumerations.Sexo)).Cast<Enumerations.Sexo>();
            comboBoxStatus.ItemsSource = Enum.GetValues(typeof(Enumerations.Status)).Cast<Enumerations.Status>();
        }
    }
}
