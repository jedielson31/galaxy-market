﻿using System.Windows.Controls;

namespace GalaxyMarket.Wpf.Validations
{
    public static class StringValidations
    {
        public static bool HasText(this TextBox textBox)
        {
            return !string.IsNullOrEmpty(textBox.Text?.Trim());
        }

        public static bool HasValueSelected(this ComboBox comboBox)
        {
            return comboBox.SelectedItem != null;
        }

        public static bool HasDateSelected(this DatePicker datePicker)
        {
            return datePicker.SelectedDate != null;
        }
    }
}
