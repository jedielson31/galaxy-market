﻿using System;
using GalaxyMarket.SharedKernel.Validation;
using TipoSexo = GalaxyMarket.Domain.Model.Enumerations.Sexo;

namespace GalaxyMarket.Domain.Model
{
    public abstract class Pessoa
    {
        protected Pessoa()
        {
            
        }

        protected Pessoa(string nome, string cpf, string cep, DateTime dataDeNascimento, TipoSexo sexo, string email)
        {
            Nome = nome;
            Cpf = cpf;
            Cep = cep;
            DataDeNascimento = dataDeNascimento;
            Sexo = sexo;
            Email = email;
        }

        public string Nome { get; protected set; }

        public string Cpf { get; protected set; }

        public string Cep { get; protected set; }

        public DateTime DataDeNascimento { get; protected set; }

        public TipoSexo Sexo { get; protected set; }

        public string Email { get; protected set; }

        protected void ValidaNomeECpf()
        {
            AssertionConcern.AssertArgumentNotNull(Nome, "O nome é obrigatório");
            AssertionConcern.AssertArgumentLength(Nome.Trim(), 3, 60, "O nome deve possuir entre 3 e 60 caracteres");

            AssertionConcern.AssertArgumentNotNull(Cpf, "O CPF é obrigatório");
            AssertionConcern.AssertArgumentLength(Cpf.Trim(),11, 11, "O CPF deve possuir 11 caracteres");
        }
    }
}