﻿using System;
using GalaxyMarket.SharedKernel.Validation;

namespace GalaxyMarket.Domain.Model
{
    public class Cliente : Pessoa
    {
        protected Cliente()
        {

        }

        public Cliente(Guid clienteId, string nome, string cpf, string cep, DateTime dataDeNascimento, Enumerations.Sexo sexo, string email)
            : base(nome, cpf, cep, dataDeNascimento, sexo, email)
        {
            ClienteId = clienteId;
        }

        public Cliente(string nome, string cpf, string cep, DateTime dataDeNascimento, Enumerations.Sexo sexo, string email)
            : this(Guid.NewGuid(), nome, cpf, cep, dataDeNascimento, sexo, email)
        {
        }

        public Guid ClienteId { get; private set; }

        public Enumerations.Status Status { get; private set; }

        public void ValidaCriacao()
        {
            ValidaNomeECpf();
            AssertionConcern.AssertDateLessThan(DataDeNascimento, DateTime.Today, "Um cliente não pode ter data de nascimento maior que a data atual");
        }

        public void InativarCliente()
        {
            Status = Enumerations.Status.Desativo;
        }

        public void AtivarCliente()
        {
            Status = Enumerations.Status.Ativo;
        }
    }
}