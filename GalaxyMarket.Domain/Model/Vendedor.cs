﻿using System;
using GalaxyMarket.SharedKernel.Validation;

namespace GalaxyMarket.Domain.Model
{
    public class Vendedor : Pessoa
    {
        private const decimal MaxTaxaComissao = 100;

        protected Vendedor()
        {
            
        }

        public Vendedor(Guid vendedorId, string nome, string cpf, string cep, DateTime dataDeNascimento, Enumerations.Sexo sexo, string email, decimal taxaDeComissao) 
            : base(nome, cpf, cep, dataDeNascimento, sexo, email)
        {
            VendedorId = vendedorId;
            TaxaDeComissao = taxaDeComissao;
        }

        public Vendedor(string nome, string cpf, string cep, DateTime dataDeNascimento, Enumerations.Sexo sexo, string email, decimal taxaDeComissao) 
            : this(Guid.NewGuid(),nome, cpf, cep, dataDeNascimento, sexo, email, taxaDeComissao)
        {
        }

        public Guid VendedorId { get; private set; }

        public decimal TaxaDeComissao { get; private set; }

        public void ValidaCriacao()
        {
            ValidaNomeECpf();
            AssertionConcern.AssertArgumentRange(TaxaDeComissao, decimal.Zero, MaxTaxaComissao, "A taxa de comissão deve ser um numero entre 0 e 100");
        }
    }
}