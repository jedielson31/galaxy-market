﻿namespace GalaxyMarket.Domain.Model
{
    public class Enumerations
    {
        public enum Sexo
        {
            Masculino = 1,
            Feminino = 2
        }

        public enum Status
        {
            Ativo = 1,
            Desativo = 2
        }

        public enum Produto
        {
            Verdura = 1,
            Legume = 2,
            Fruta = 3,
            Animal = 4,
            Bebida = 5
        }
    }
}
