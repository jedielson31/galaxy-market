﻿using System;
using TipoProduto = GalaxyMarket.Domain.Model.Enumerations.Produto;

namespace GalaxyMarket.Domain.Model
{
    public class Produto
    {
        protected Produto()
        {

        }

        public Produto(Guid produtoId, string nome, Enumerations.Produto tipo, decimal valorCompra, decimal valorMarkup ) 
        {

        }

        public Guid ProdutoId { get; private set; }
        public string Nome { get; private set; }
        public TipoProduto Tipo { get; private set; }
        public decimal ValorCompra { get; private set; }
        public decimal VarlorMarkup { get; private set; }

    }
}