﻿using System;
using System.Collections.Generic;
using GalaxyMarket.Domain.Filters;
using GalaxyMarket.Domain.Interfaces.Repository;
using GalaxyMarket.Domain.Interfaces.Services;
using GalaxyMarket.Domain.Model;

namespace GalaxyMarket.Domain.Services
{
    public class ClienteService : IClienteService
    {
        private bool _disposed;

        private readonly IClienteRepository _clienteRepository;

        public ClienteService(IClienteRepository clienteRepository)
        {
            _clienteRepository = clienteRepository;
        }

        public void Create(Cliente entity)
        {
            try
            {
                entity.ValidaCriacao();

                if (_clienteRepository.BuscarPorCpf(entity.Cpf) != null)
                {
                    throw new Exception("Já existe um cliente com este CPF");
                }
                
                entity.AtivarCliente();
                _clienteRepository.Save(entity);
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao salvar o cliente",ex);
            }
        }

        public IEnumerable<Cliente> BuscarFiltrado(ClienteFilter filtro)
        {
            return _clienteRepository.BuscarFiltrado(filtro);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~ClienteService()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                // free other managed objects that implement
                // IDisposable only
                _clienteRepository.Dispose();
            }

            // release any unmanaged objects
            // set the object references to null

            _disposed = true;
        }
    }
}