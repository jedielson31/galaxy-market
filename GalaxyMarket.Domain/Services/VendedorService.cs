﻿using System;
using GalaxyMarket.Domain.Interfaces.Repository;
using GalaxyMarket.Domain.Interfaces.Services;
using GalaxyMarket.Domain.Model;

namespace GalaxyMarket.Domain.Services
{
    public class VendedorService : IVendedorService
    {
        private bool _disposed;

        private readonly IVendedorRepository _vendedorRepository;

        public VendedorService(IVendedorRepository vendedorRepository)
        {
            _vendedorRepository = vendedorRepository;
        }

        public void Create(Vendedor entity)
        {
            try
            {
                entity.ValidaCriacao();

                if (_vendedorRepository.BuscarPorCpf(entity.Cpf) != null)
                {
                    throw new Exception("Já existe um cliente com este CPF");
                }

                _vendedorRepository.Save(entity);
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao salvar o vendedor", ex);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~VendedorService()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                // free other managed objects that implement
                // IDisposable only
                _vendedorRepository.Dispose();
            }

            // release any unmanaged objects
            // set the object references to null

            _disposed = true;
        }
    }
}