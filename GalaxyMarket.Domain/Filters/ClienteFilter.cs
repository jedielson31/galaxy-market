﻿using System;
using GalaxyMarket.Domain.Model;

namespace GalaxyMarket.Domain.Filters
{
    public class ClienteFilter
    {
        public string Nome { get;  set; }

        public string Cpf { get;  set; }

        public string Cep { get;  set; }

        public DateTime? DataDeNascimentoInicial { get;  set; }

        public DateTime? DataDeNascimentoFinal { get;  set; }

        public Enumerations.Sexo? Sexo { get;  set; }

        public Enumerations.Status? Status { get; set; }

        public string Email { get;  set; }
    }
}