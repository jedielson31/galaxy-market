﻿using System;
using System.Collections.Generic;
using GalaxyMarket.Domain.Filters;
using GalaxyMarket.Domain.Model;

namespace GalaxyMarket.Domain.Interfaces.Repository
{
    public interface IClienteRepository : IDisposable
    {
        /// <summary>
        /// Persisite um <see cref="Cliente"/>
        /// </summary>
        /// <param name="entity">O <see cref="Cliente"/> a ser persistido</param>
        void Save(Cliente entity);

        /// <summary>
        /// Retorna um <see cref="Cliente"/> tal que seu cpf seja igual a <param name="cpf"/>
        /// <para>Caso não encontre, retorna NULL</para>
        /// </summary>
        /// <param name="cpf">O cpf a ser pesquisado</param>
        /// <returns>Um <see cref="Cliente"/></returns>
        Cliente BuscarPorCpf(string cpf);

        /// <summary>
        /// Retorna uma coleção de clientes baseada num filtro
        /// </summary>
        /// <param name="filtro">O filtro a ser aplicado</param>
        /// <returns>Uma coleção de clientes</returns>
        IEnumerable<Cliente> BuscarFiltrado(ClienteFilter filtro);
    }
}