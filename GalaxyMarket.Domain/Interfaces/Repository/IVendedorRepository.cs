﻿using System;
using GalaxyMarket.Domain.Model;

namespace GalaxyMarket.Domain.Interfaces.Repository
{
    public interface IVendedorRepository : IDisposable
    {
        /// <summary>
        /// Persisite um <see cref="Vendedor"/>
        /// </summary>
        /// <param name="entity">O <see cref="Vendedor"/> a ser persistido</param>
        void Save(Vendedor entity);

        /// <summary>
        /// Retorna um <see cref="Vendedor"/> tal que seu cpf seja igual a <param name="cpf"/>
        /// <para>Caso não encontre, retorna NULL</para>
        /// </summary>
        /// <param name="cpf">O cpf a ser pesquisado</param>
        /// <returns>Um <see cref="Vendedor"/></returns>
        Vendedor BuscarPorCpf(string cpf);
    }
}