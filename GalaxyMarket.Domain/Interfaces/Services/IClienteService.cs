﻿using System;
using System.Collections.Generic;
using GalaxyMarket.Domain.Filters;
using GalaxyMarket.Domain.Model;

namespace GalaxyMarket.Domain.Interfaces.Services
{
    public interface IClienteService : IDisposable
    {
        void Create(Cliente entity);

        /// <summary>
        /// Retorna uma coleção de clientes baseada num filtro
        /// </summary>
        /// <param name="filtro">O filtro a ser aplicado</param>
        /// <returns>Uma coleção de clientes</returns>
        IEnumerable<Cliente> BuscarFiltrado(ClienteFilter filtro);
    }
}