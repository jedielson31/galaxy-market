﻿using System;
using GalaxyMarket.Domain.Model;

namespace GalaxyMarket.Domain.Interfaces.Services
{
    public interface IVendedorService : IDisposable
    {
        void Create(Vendedor entity);
    }
}