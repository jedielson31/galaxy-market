﻿using System;
using GalaxyMarket.Application;
using GalaxyMarket.Domain.Model;
using GalaxyMarket.Domain.Services;
using GalaxyMarket.Infra.Data.Context;
using GalaxyMarket.Infra.Data.Repository.EntityFramework;

namespace GalaxyMarket.ShellView
{
    class Program
    {
        static void Main(string[] args)
        {
            var repo = new ClienteRepository();
            var service = new ClienteService(repo);
            var appService = new ClienteAppService(service);

            try
            {
                var cliente = new Cliente("Darth Vader", "55555555579", "Ronaldo", new DateTime(1988, 3, 31), Enumerations.Sexo.Masculino,
                    "darth.vader@sith.com");
                appService.Create(cliente);
                Console.WriteLine("Operação executada com sucesso.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " - " + ex.InnerException.Message);
            }
            finally
            {
                appService.Dispose();
            }

            Console.ReadKey();
        }
    }
}
