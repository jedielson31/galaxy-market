﻿using System;
using GalaxyMarket.Domain.Model;

namespace GalaxyMarket.Application.Interfaces
{
    public interface IVendedorAppService : IDisposable
    {
        /// <summary>
        /// Cria um novo <see cref="Vendedor"/>
        /// </summary>
        /// <param name="entity">O vendedor a ser criado</param>
        void Create(Vendedor entity);
    }
}