﻿using System;
using GalaxyMarket.Domain.Model;

namespace GalaxyMarket.Application.Interfaces
{
    public interface IClienteAppService : IDisposable
    {
        /// <summary>
        /// Cria um novo <see cref="Cliente"/>
        /// </summary>
        /// <param name="entity">O Cliente a ser criado</param>
        void Create(Cliente entity);
    }
}