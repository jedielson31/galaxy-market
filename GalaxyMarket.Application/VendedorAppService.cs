﻿using System;
using GalaxyMarket.Application.Interfaces;
using GalaxyMarket.Domain.Interfaces.Services;
using GalaxyMarket.Domain.Model;

namespace GalaxyMarket.Application
{
    public class VendedorAppService : IVendedorAppService
    {
        private bool _disposed;

        private readonly IVendedorService _vendedorService;

        public VendedorAppService(IVendedorService vendedorService)
        {
            _vendedorService = vendedorService;
        }

        public void Create(Vendedor entity)
        {
            _vendedorService.Create(entity);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~VendedorAppService()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                // free other managed objects that implement
                // IDisposable only
                _vendedorService.Dispose();
            }

            // release any unmanaged objects
            // set the object references to null

            _disposed = true;
        }
    }
}