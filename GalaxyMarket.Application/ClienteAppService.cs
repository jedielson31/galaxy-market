﻿using System;
using GalaxyMarket.Application.Interfaces;
using GalaxyMarket.Domain.Interfaces.Services;
using GalaxyMarket.Domain.Model;

namespace GalaxyMarket.Application
{
    public class ClienteAppService : IClienteAppService
    {
        private bool _disposed;

        private readonly IClienteService _clienteService;

        public ClienteAppService(IClienteService clienteService)
        {
            _clienteService = clienteService;
        }

        public void Create(Cliente entity)
        {
            _clienteService.Create(entity);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~ClienteAppService()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                // free other managed objects that implement
                // IDisposable only
                _clienteService.Dispose();
            }

            // release any unmanaged objects
            // set the object references to null

            _disposed = true;
        }
    }
}
