﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using GalaxyMarket.Domain.Model;

namespace GalaxyMarket.Infra.Data.Context.Mapping
{
    public class ClienteMap : EntityTypeConfiguration<Cliente>
    {
        public ClienteMap()
        {
            HasKey(c => c.ClienteId);
            Property(c => c.ClienteId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(c => c.Cpf).IsRequired().HasMaxLength(11);
            Property(c => c.Nome).IsRequired().HasMaxLength(60);
            Property(c => c.Cep).HasMaxLength(8);
        }
    }
}