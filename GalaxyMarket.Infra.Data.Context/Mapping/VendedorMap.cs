﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using GalaxyMarket.Domain.Model;

namespace GalaxyMarket.Infra.Data.Context.Mapping
{
    public class VendedorMap : EntityTypeConfiguration<Vendedor>
    {
        public VendedorMap()
        {
            HasKey(v => v.VendedorId);
            Property(v => v.VendedorId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(v => v.Cpf).IsRequired().HasMaxLength(11);
            Property(v => v.Cep).HasMaxLength(8);
            Property(v => v.Nome).IsRequired().HasMaxLength(60);
            Property(v => v.TaxaDeComissao).IsRequired();
        }
    }
}