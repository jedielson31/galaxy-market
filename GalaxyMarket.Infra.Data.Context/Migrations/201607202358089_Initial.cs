namespace GalaxyMarket.Infra.Data.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cliente",
                c => new
                    {
                        ClienteId = c.Guid(nullable: false, identity: true),
                        Status = c.Int(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 60, unicode: false),
                        Cpf = c.String(nullable: false, maxLength: 11, unicode: false),
                        Cep = c.String(maxLength: 100, unicode: false),
                        Idade = c.Int(nullable: false),
                        Sexo = c.Int(nullable: false),
                        Email = c.String(maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.ClienteId);
            
            CreateTable(
                "dbo.Vendedor",
                c => new
                    {
                        VendedorId = c.Guid(nullable: false, identity: true),
                        TaxaDeComissao = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Nome = c.String(nullable: false, maxLength: 60, unicode: false),
                        Cpf = c.String(nullable: false, maxLength: 11, unicode: false),
                        Cep = c.String(maxLength: 100, unicode: false),
                        Idade = c.Int(nullable: false),
                        Sexo = c.Int(nullable: false),
                        Email = c.String(maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.VendedorId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Vendedor");
            DropTable("dbo.Cliente");
        }
    }
}
