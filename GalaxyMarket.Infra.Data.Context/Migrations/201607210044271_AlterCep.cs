namespace GalaxyMarket.Infra.Data.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterCep : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Cliente", "Cep", c => c.String(maxLength: 8, unicode: false));
            AlterColumn("dbo.Vendedor", "Cep", c => c.String(maxLength: 8, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Vendedor", "Cep", c => c.String(maxLength: 100, unicode: false));
            AlterColumn("dbo.Cliente", "Cep", c => c.String(maxLength: 100, unicode: false));
        }
    }
}
