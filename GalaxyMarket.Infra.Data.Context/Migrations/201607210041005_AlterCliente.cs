namespace GalaxyMarket.Infra.Data.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterCliente : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cliente", "DataDeNascimento", c => c.DateTime(nullable: false));
            AddColumn("dbo.Vendedor", "DataDeNascimento", c => c.DateTime(nullable: false));
            DropColumn("dbo.Cliente", "Idade");
            DropColumn("dbo.Vendedor", "Idade");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Vendedor", "Idade", c => c.Int(nullable: false));
            AddColumn("dbo.Cliente", "Idade", c => c.Int(nullable: false));
            DropColumn("dbo.Vendedor", "DataDeNascimento");
            DropColumn("dbo.Cliente", "DataDeNascimento");
        }
    }
}
