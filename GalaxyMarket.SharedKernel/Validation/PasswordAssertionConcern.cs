﻿namespace GalaxyMarket.SharedKernel.Validation
{
    public class PasswordAssertionConcern
    {
        public static void AssertIsValid(string password, string errorMessage)
        {
            AssertionConcern.AssertArgumentNotNull(password, errorMessage);
        }
    }
}