﻿using System;
using System.Data.Entity;
using System.Linq;
using GalaxyMarket.Domain.Interfaces.Repository;
using GalaxyMarket.Domain.Model;
using GalaxyMarket.Infra.Data.Context;

namespace GalaxyMarket.Infra.Data.Repository.EntityFramework
{
    public class VendedorRepository : IVendedorRepository
    {
        private bool _disposed;
        private readonly GalaxyMarketContext _context;

        public VendedorRepository()
        {
            _context = new GalaxyMarketContext();
            if (_context == null)
            {
                throw new Exception("O contexto é obrigatório");
            }
            DbSet = _context.Vendedores;
        }

        public DbSet<Vendedor> DbSet { get; set; }

        public void Save(Vendedor entity)
        {
            DbSet.Add(entity);
            _context.SaveChanges();
        }

        public Vendedor BuscarPorCpf(string cpf)
        {
            return DbSet.AsNoTracking()
                .FirstOrDefault(v => v.Cpf == cpf);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~VendedorRepository()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                // free other managed objects that implement
                // IDisposable only
                _context.Dispose();
            }

            // release any unmanaged objects
            // set the object references to null

            _disposed = true;
        }
    }
}