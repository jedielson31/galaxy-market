﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using GalaxyMarket.Domain.Filters;
using GalaxyMarket.Domain.Interfaces.Repository;
using GalaxyMarket.Domain.Model;
using GalaxyMarket.Infra.Data.Context;
using LinqKit;

namespace GalaxyMarket.Infra.Data.Repository.EntityFramework
{
    public class ClienteRepository : IClienteRepository
    {
        private bool _disposed;

        private readonly GalaxyMarketContext _context;

        public ClienteRepository()
        {
            _context = new GalaxyMarketContext();
            if (_context == null)
            {
                throw new Exception("O contexto não pode ser nulo");
            }

            DbSet = _context.Clientes;
        }

        public DbSet<Cliente> DbSet { get; set; }

        public void Save(Cliente entity)
        {
            DbSet.Add(entity);
            _context.SaveChanges();
        }

        public Cliente BuscarPorCpf(string cpf)
        {
            var retorno = DbSet
                .AsNoTracking()
                .FirstOrDefault(c => c.Cpf == cpf);
            return retorno;
        }

        public IEnumerable<Cliente> BuscarFiltrado(ClienteFilter filtro)
        {
            if (filtro == null)
            {
                throw new ArgumentNullException(nameof(filtro));
            }

            var predicate = PredicateBuilder.True<Cliente>();

            if (!string.IsNullOrEmpty(filtro.Nome?.Trim()))
            {
                predicate = predicate.And(c => c.Nome.Contains(filtro.Nome));
            }

            if (!string.IsNullOrEmpty(filtro.Cpf?.Trim()))
            {
                predicate = predicate.And(c => c.Cpf == filtro.Cpf);
            }

            if (!string.IsNullOrEmpty(filtro.Cep?.Trim()))
            {
                predicate = predicate.And(c => c.Cep == filtro.Cep);
            }

            if (!string.IsNullOrEmpty(filtro.Email?.Trim()))
            {
                predicate = predicate.And(c => c.Email == filtro.Email);
            }

            if (filtro.Status != null)
            {
                predicate = predicate.And(c => c.Status == filtro.Status);
            }

            if (filtro.Sexo != null)
            {
                predicate = predicate.And(c => c.Sexo == filtro.Sexo);
            }

            if (filtro.DataDeNascimentoInicial != null)
            {
                predicate = predicate.And(c => c.DataDeNascimento >= filtro.DataDeNascimentoInicial);
            }

            if (filtro.DataDeNascimentoFinal != null)
            {
                predicate = predicate.And(c => c.DataDeNascimento <= filtro.DataDeNascimentoFinal);
            }

            return DbSet
                    .AsNoTracking()
                    .Where(predicate)
                    .ToList();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~ClienteRepository()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                // free other managed objects that implement
                // IDisposable only
                _context.Dispose();
            }

            // release any unmanaged objects
            // set the object references to null

            _disposed = true;
        }

    }
}